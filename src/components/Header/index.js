import React from "react";

import "./Header.css";

const Header = ({ text, classNames }) => {
    return(
        <header className={`header ${classNames}`}>
            <h1>{text}</h1>
        </header>
    );
}

export default Header;