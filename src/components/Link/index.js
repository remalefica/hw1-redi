import React from "react";

import './Link.css';

const Link = ({ children, link, classNames }) => (
    <a href={link} className={`link ${classNames}`}>{children}</a>
);

export default Link;