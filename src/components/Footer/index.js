import React from "react";

import Link from "../Link";

import "./Footer.css";

const Footer = ({ subText, actionText, classNames }) => (
    <footer className={`${classNames}`}>
        <p className={"footer__sub-text"}>{subText}</p>
        <Link link={"#"} classNames={"footer__link"}>{actionText}</Link>
    </footer>
);

export default Footer;