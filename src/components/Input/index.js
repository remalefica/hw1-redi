import React from "react";

import "./Input.css";

const Input = ({ type, value, placeholder, changeInputValue, classNames }) => {
    return(
        <input type={type}
               onChange={({ target }) => changeInputValue(target.value)}
               value={value}
               placeholder={placeholder}
               className={`input ${classNames}`}
        />
    );
}

export default Input;